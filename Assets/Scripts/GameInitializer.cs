using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    public static GameInitializer Instance;
    [SerializeField] private GameObject _mainScreen;
    [SerializeField] private GameInitializerSettings _settings;

    private List<Transform> _levelList;
    private const string savedLevelKey = "SavedLevelKey";
    private LevelData _levelData;
    private IDataSaver _dataSaver;

    private void Awake()
    {
        Instance = this;
        _dataSaver = new DataSaver();
        _levelList = _settings.LevelsList;
        _levelData = _dataSaver.Load<LevelData>(savedLevelKey);

        if (_levelData == null)
        {
            _levelData = new LevelData();
        }
    }

    private void Start()
    {
        Instantiate(_mainScreen, transform);
    }

    public void StartWithSavedLevel()
    {
        if (_levelData.SavedLevelID < _levelList.Count)
        {
            StartNextLevel();
        }
        else
        {
            _levelData.SavedLevelID = 0;
            StartLevelById(_levelData.SavedLevelID);
        }
    }

    public void StartNextLevel()
    {
        StartLevelById(_levelData.SavedLevelID);
    }

    public void NextLevel()
    {
        _levelData.SavedLevelID++;
    }

    public void SaveProgress()
    {
        _dataSaver.Save(savedLevelKey, _levelData);
    }

    public int GetLastLevlId()
    {
        return _levelData.SavedLevelID;
    }

    public void RestartCurrentLevel()
    {
        StartWithSavedLevel();
    }

    private void StartLevelById(int id)
    {
        if (id < _levelList.Count)
        {
            CreateLevel(id);
        }
        else
        {
            StartWithSavedLevel();
        }

    }

    private void CreateLevel(int id)
    {
        Instantiate(
            _levelList[id],
            _levelList[id].position,
            _levelList[id].rotation
        );
    }

}
