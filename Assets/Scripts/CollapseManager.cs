using System.Collections;
using UnityEngine;

public class CollapseManager : MonoBehaviour
{
    public static CollapseManager Instance;
    [SerializeField] private LevelManager _levelManager;

    private void Awake()
    {
        Instance = this;
    }

    public void Collapse(ActiveItem itemA, ActiveItem itemB)
    {
        StartCoroutine(CollapseProcess(itemA, itemB));        
    }

    private IEnumerator CollapseProcess(ActiveItem itemA, ActiveItem itemB)
    {
        itemA.Disable();

        var startPosition = itemA.transform.position;

        for (float t = 0; t < 1f; t += Time.deltaTime / 0.08f)
        {
            itemA.transform.position = Vector3.Lerp(startPosition, itemB.transform.position, t);
            yield return null;
        }

        itemA.transform.position = itemB.transform.position;
        itemA.Die();
        itemB.IncreaseLevel();
        
        _levelManager.CheckBallTask(itemB.GetComponent<ActiveItem>().Level);

        ExplodeBall(itemB.transform.position, itemB.Radius + 0.15f);
    }

    public void ExplodeBall(Vector3 position, float radius)
    {
        Collider[] colliders = Physics.OverlapSphere(position, radius);

        for (int i = 0; i < colliders.Length; i++)

        {
            var passiveItems = colliders[i].GetComponent<PassiveItem>();

            if (colliders[i].attachedRigidbody)
            {                
                passiveItems = colliders[i].attachedRigidbody.GetComponent<PassiveItem>();
            }

            if (passiveItems)
            {
                passiveItems.OnAffect();
            }
        }
    }
}
