
public interface IDataSaver
{
    void Save<T>(string key, T val);

    T Load<T>(string key);
}
