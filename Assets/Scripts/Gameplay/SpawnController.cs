using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField] private float _sencentivity = 25f;
    [SerializeField] private float _maxPositionX = 2.5f;

    private float _positionX;
    private float _oldMouseX;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _oldMouseX = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0))
        {
            Move();
        }
    }

    private void Move()
    {
        var offset = Input.mousePosition.x - _oldMouseX;

        _oldMouseX = Input.mousePosition.x;
        _positionX += offset * _sencentivity / Screen.width;
        _positionX = Mathf.Clamp(_positionX, -_maxPositionX, _maxPositionX);
        transform.position = new Vector3(_positionX, transform.position.y, transform.position.z);
    }
}
