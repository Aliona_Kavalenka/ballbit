using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private Material _skyboxMaterial;

    private void Start()
    {
        SetBackground();
    }

    private void SetBackground()
    {
        RenderSettings.skybox = _skyboxMaterial;
    }

    public static void PauseGame(bool pause)
    {
        if (pause)
        {            
            Time.timeScale = 0;
        }
        else if (!pause)
        {
            Time.timeScale = 1;
        }
    }

}
