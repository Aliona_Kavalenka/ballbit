using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct Task
{
    public ItemType ItemType;
    public Animator Animation;
    public int Level;
    public int Count;
    public Text CountText;
}

public class LevelManager : MonoBehaviour
{
    [SerializeField] private int _ballCountInLevel = 50;
    [SerializeField] private Text _ballCountText;
    [SerializeField] private Task[] Tasks;
    [SerializeField] private GameObject _gameScreen;
    [SerializeField] private GameObject _loseScreen;
    [SerializeField] private GameObject _winScreen;

    private GameObject _lose;
    private GameObject _win;

    private void Start()
    {
        SetupTasks();

        _lose = Instantiate(_loseScreen);
        _win = Instantiate(_winScreen);

        _lose.SetActive(false);
        _win.SetActive(false);

        _ballCountText.text = _ballCountInLevel.ToString();
        _gameScreen.SetActive(false);
    }

    private void Update()
    {
        if (Time.timeScale != 0 )
        {
            _gameScreen.SetActive(true);
        }
        else
        {
            _gameScreen.SetActive(false);
        }

        if (_ballCountInLevel <= 0)
        {
            _ballCountInLevel = 50;

            _lose.SetActive(true);
            DisableCurrentLevel();
        }
    }

    public void ReduceBallCount()
    {
        _ballCountInLevel--;
        _ballCountText.text = _ballCountInLevel.ToString();
    }

    private void SetupTasks()
    {
        for (int i = 0; i < Tasks.Length; i++)
        {
            Tasks[i].CountText.text = Tasks[i].Count.ToString();
        }
    }

    public void CheckBallTask(int level)
    {
        for (int i = 0; i < Tasks.Length; i++)
        {
            if (Tasks[i].Level == level && Tasks[i].Count > 0)
            {
                Tasks[i].Count -= 1;
                Tasks[i].Animation.SetTrigger("Task");
                SetupTasks();

                if (ChekTasksCompleted())
                {
                    _win.SetActive(true);
                    DisableCurrentLevel();
                }
            }
        }        
    }

    public void CheckOtherTask(ItemType itemType)
    {
        for (int i = 0; i < Tasks.Length; i++)
        {
            if (Tasks[i].ItemType == itemType && Tasks[i].Count > 0)
            {
                Tasks[i].Count -= 1;
                Tasks[i].Animation.SetTrigger("Task");
                SetupTasks();

                if (ChekTasksCompleted())
                {
                    _win.SetActive(true);
                    DisableCurrentLevel();
                }
            }
        }
    }

    private bool ChekTasksCompleted()
    {
        var allTasksCount = Tasks.Length;
        int completedTasks = 0;

        for (int i = 0; i < Tasks.Length; i++)
        {
            if (Tasks[i].Count <= 0)
            {
                completedTasks++;
            }
        }
        
        return completedTasks == allTasksCount ? true : false;
    }

    private void DisableCurrentLevel()
    {
        transform.GetComponentInParent<Transform>().root.gameObject.SetActive(false);
    }

}
