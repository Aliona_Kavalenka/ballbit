using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ItemType
{
    Ball,
    Box,
    Barrel,
    Stone
}

public class Item : MonoBehaviour
{
    public ItemType ItemType;
}
