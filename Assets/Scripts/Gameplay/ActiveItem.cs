using UnityEngine;
using UnityEngine.UI;

public class ActiveItem : MonoBehaviour
{
    public int Level;
    public Rigidbody Rigidbody;
    public Projection Projection;
    [HideInInspector] public float Radius;    

    [SerializeField] protected Text _levelText;
    [SerializeField] private Transform _visualTransform;
    [SerializeField] private SphereCollider _collider;
    [SerializeField] private SphereCollider _trigger;
    [SerializeField] private float _ballSpeed = 1.25f;
    [SerializeField] private Animator _animator;

    private bool _isDead;

    [ContextMenu("IncreaseLevel")]
    public void IncreaseLevel()
    {
        Level++;
        SetLevel(Level);
        _animator.SetTrigger("IncreaseLevel");

        _trigger.enabled = false;
        _trigger.enabled = true;
    }

    private void Start()
    {
        Projection.Hide();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isDead) return;

        if (other.attachedRigidbody)
        {
            var otherItem = other.attachedRigidbody.GetComponent<ActiveItem>();

            if (otherItem)
            {
                if (!otherItem._isDead && Level == otherItem.Level)
                {
                    CollapseManager.Instance.Collapse(this, otherItem);
                }
            }
        }
    }

    public virtual void SetLevel(int level)
    {
        Level = level;

        var number = (int)Mathf.Pow(2, level + 1);
        var numText = number.ToString();

        _levelText.text = numText;

        Radius = Mathf.Lerp(0.4f, 0.7f, level / 10f);

        var ballScale = Vector3.one * Radius * 2f;

        _visualTransform.localScale = ballScale;
        _collider.radius = Radius;
        _trigger.radius = Radius + 0.1f;
    }

    public void SetupToTube()
    {
        _trigger.enabled = false;
        _collider.enabled = false;
        Rigidbody.isKinematic = true;
        Rigidbody.interpolation = RigidbodyInterpolation.None;
    }

    public void DropBall()
    {
        _trigger.enabled = true;
        _collider.enabled = true;
        Rigidbody.isKinematic = false;
        Rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        transform.parent = null;
        Rigidbody.velocity = Vector3.down * _ballSpeed;
    }

    public void Disable()
    {
        _trigger.enabled = false;
        Rigidbody.isKinematic = true;
        _collider.enabled = false;
        _isDead = true;
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
