using UnityEngine;

public class Barrel : PassiveItem 
{
    [SerializeField] private GameObject _barrelExposition;

    public override void OnAffect()
    {
        base.OnAffect();
        Die();
    }

    public override void Die()
    {
        base.Die();

        Instantiate(_barrelExposition, transform.position, Quaternion.Euler(-90f,0f,0f));
        Destroy(gameObject);
    }
}
