using UnityEngine;

public class Stone : PassiveItem
{
    [Range(0, 2)]
    [SerializeField] private int _level = 2;
    [SerializeField] private Stone _stonePref;
    [SerializeField] private Transform _visualTransform;    
    [SerializeField] private GameObject _dieEffect;

    public override void OnAffect()
    {
        base.OnAffect();

        if (_level > 0)
        {
            for (int i = 0; i < 2; i++)
            {
                CreateChildRock(_level - 1);
            }
        }

        Die();
    }

    private void CreateChildRock(int level)
    {
        var newRock = Instantiate(_stonePref, transform.position, Quaternion.identity);
        newRock.SetLevel(level);
        newRock.transform.parent = transform.parent;
    }

    public void SetLevel(int level)
    {
        _level = level;
        var scale = 1f;

        if (level == 2)
        {
            scale = 1f;
        }
        else if (level == 1)
        {
            scale = 0.7f;
        }
        else if (level == 0)
        {
            scale = 0.45f;
        }

        _visualTransform.localScale = Vector3.one * scale;
    }

    public override void Die()
    {
        base.Die();

        Instantiate(_dieEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
