using UnityEngine;

public class PassiveItem : MonoBehaviour
{
    [SerializeField] protected LevelManager _levelManager;

    public virtual void OnAffect()
    {

    }

    public virtual void Die()
    {
        _levelManager.CheckOtherTask(gameObject.GetComponent<Item>().ItemType);
    }
}
