using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSaver : IDataSaver
{
    public void Save<T>(string key, T val)
    {
        var json = JsonUtility.ToJson(val);

        PlayerPrefs.SetString(key, json);
        PlayerPrefs.Save();
    }

    public T Load<T>(string key)
    {
        var json = PlayerPrefs.GetString(key);
        return JsonUtility.FromJson<T>(json);
    }
}
