using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour
{
    [SerializeField] private Text _levelText;

    private void Start()
    {
        GameInitializer.Instance.NextLevel();
        GameInitializer.Instance.SaveProgress();
        LevelController.PauseGame(true);
        _levelText.text = GameInitializer.Instance.GetLastLevlId().ToString();
    }

    private void Hide()
    {
        gameObject.SetActive(false);
        LevelController.PauseGame(false);
    }

    public void NextLevel()
    {
        GameInitializer.Instance.StartNextLevel();
        Hide();
    }
}
