using UnityEngine;
using UnityEngine.UI;

public class MainScreen : MonoBehaviour
{
    [SerializeField] private Text _levelText;

    private void Start()
    {
        LevelController.PauseGame(true);
        _levelText.text = (GameInitializer.Instance.GetLastLevlId() + 1).ToString();
    }

    private void Hide()
    {        
        gameObject.SetActive(false);
        LevelController.PauseGame(false);
    }

    public void StartGame()
    {
        Hide();
        GameInitializer.Instance.StartWithSavedLevel();
    }
}
