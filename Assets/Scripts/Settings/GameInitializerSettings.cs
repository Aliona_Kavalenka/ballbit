using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameInitializerSettings", menuName = "Settings/GameInitializerSettings")]
public class GameInitializerSettings : ScriptableObject
{
    [SerializeField] private List<Transform> _levelsList = new List<Transform>();

    public List<Transform> LevelsList => _levelsList;
}
