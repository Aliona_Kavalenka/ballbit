using UnityEngine;

[CreateAssetMenu(fileName = "BallSettings", menuName = "Settings/BallSettings")]
public class BallSettings : ScriptableObject
{
    public Material[] BallMaterials;
    public Material[] BallProjectionMaterials;
}
